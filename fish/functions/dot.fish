function dot --wraps='cd $HOME/dotfiles/ & ls -la' --wraps='cd $HOME/dotfiles/ & ls -lah' --wraps='cd $HOME/dotfiles/ & ls' --description 'alias dot=cd $HOME/dotfiles/ & ls'
  cd $HOME/dotfiles/ & ls $argv; 
end
