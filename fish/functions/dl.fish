function dl --wraps='cd downloads' --wraps='cd $HOME/downloads' --description 'alias dl=cd $HOME/downloads'
  cd $HOME/downloads $argv; 
end
