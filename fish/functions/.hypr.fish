function .hypr --wraps='cd $HOME/dotfiles/hypr/ & v hyprland.cfg' --wraps='cd $HOME/dotfiles/hypr/ & v hyprland.conf' --description 'alias .hypr=cd $HOME/dotfiles/hypr/ & v hyprland.conf'
  cd $HOME/dotfiles/hypr/ & v hyprland.conf $argv; 
end
