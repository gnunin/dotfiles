function din --wraps='sudo dnf --best install' --description 'alias din=sudo dnf --best install'
  sudo dnf --best install $argv; 
end
