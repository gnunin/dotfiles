function .alacritty --wraps='cd $HOME/dotfiles/alacritty/ && nvim alacritty.yml' --description 'alias .alacritty=cd $HOME/dotfiles/alacritty/ && nvim alacritty.yml'
  cd $HOME/dotfiles/alacritty/ && nvim alacritty.yml $argv; 
end
