function cmx --wraps='chmod +x' --description 'alias cmx=chmod +x'
  chmod +x $argv; 
end
