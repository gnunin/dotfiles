function cl --wraps='clear && rxfetch' --wraps='clear && ./$HOME/software/bunnyfetch/bunnyfetch' --wraps='clear && sh $HOME/software/bunnyfetch/bunnyfetch' --wraps='clear && bunnyfetch;' --wraps='clear && bunnyfetch' --description 'alias cl=clear && rxfetch'
  clear && rxfetch $argv; 
end
