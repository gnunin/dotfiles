function lsa --wraps='exa -lahm --icons' --description 'alias lsa=exa -lahm --icons'
  exa -lahm --icons $argv; 
end
