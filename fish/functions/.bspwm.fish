function .bspwm --wraps='cd $HOME/dotfiles/bspwm/ & v bspwmrc' --description 'alias .bspwm=cd $HOME/dotfiles/bspwm/ & v bspwmrc'
  cd $HOME/dotfiles/bspwm/ & v bspwmrc $argv; 
end
