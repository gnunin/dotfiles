function ls --wraps='exa -lhm --icons' --description 'alias ls=exa -lhm --icons'
  exa -lhm --icons $argv; 
end
