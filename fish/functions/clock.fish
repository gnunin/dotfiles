function clock --wraps='sh $HOME/time.sh' --wraps='$HOME/scripts & sh time.sh' --description 'alias clock=$HOME/scripts & sh time.sh'
  $HOME/scripts & sh time.sh $argv; 
end
