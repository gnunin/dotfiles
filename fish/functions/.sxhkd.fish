function .sxhkd --wraps='clear && cd /home/abe/.config/sxhkd/ && ls -la' --wraps='cd $HOME/dotfiles/.config/sxhkd/ & v sxhkdrc' --wraps='cd $HOME/dotfiles/sxhkd/ & v sxhkdrc' --description 'alias .sxhkd=cd $HOME/dotfiles/sxhkd/ & v sxhkdrc'
  cd $HOME/dotfiles/sxhkd/ & v sxhkdrc $argv; 
end
