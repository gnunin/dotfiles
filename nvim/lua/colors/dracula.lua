return {
  base00 = "#282a36",
  base01 = "#3a3c4e",
  base02 = "#4d4f68",
  base03 = "#626483",
  base04 = "#8be9fd",
  base05 = "#e9e9f4",
  base06 = "#f1f8f2",
  base07 = "#f7f7fb",
  base08 = "#ff79c6",
  base09 = "#bd93f9",
  base0A = "#50fa7b",
  base0B = "#f1fa8c",
  base0C = "#8be9fd",
  base0D = "#8be9fd",
  base0E = "#bd93f9",
  base0F = "#50fa7b"
}
